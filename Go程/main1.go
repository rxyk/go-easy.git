package main
import (
	"fmt"
	"time"
	// "runtime"
)
func test(j int)  {
	fmt.Printf("  子子go程%d暂停1s\n",j)
	time.Sleep(time.Second)
	fmt.Printf("  子子go程%d结束\n",j)
}
func main() {
	go func() {
		for j := 0; j < 3; j++ {
			go test(j)
		}
		fmt.Println(" 子go程暂停1s")
		time.Sleep(time.Second)
		fmt.Println(" 子go程结束")
		// 不管是return  还是runtime.Goexit()，
		// 还是什么都没有，Go程及其栈，在函数退出时均会销毁
		// return 会让Go程马上结束，后面的代码不会再执行
		// 而什么也不写，默认执行到函数体代码的最后一行
		// runtime.Goexit()
	}()
	println("主程暂停")
	time.Sleep(time.Second*5)
	println("主程结束")
}