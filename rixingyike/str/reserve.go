package str

import (
	"flag"
	"fmt"

	"github.com/kataras/iris/v12"
)

// Fibonacci2 ...
func Fibonacci2() func() int {
	a, b := 0, 1
	return func() int {
			a, b = b, a+b
			return a
	}
}

// Fibonacci 此函数计算斐波那契数列中第 N 个数字
// 0，1，1，2，3，5，8，13
func Fibonacci(n int) int {
	switch n<2 {
	case true:
					return n
	default:
					return Fibonacci(n-1) + Fibonacci(n-2)
	}
}

// Reverse 将其实参字符串以符文为单位左右反转
func Reverse(s string) string {
	r := []rune(s)
	for i, j := 0, len(r)-1; i < len(r)/2; i, j = i+1, j-1 {
		r[i], r[j] = r[j], r[i]
	}
	fmt.Printf("inner:%s\n",string(r))
	return string(r)
}

// StartServer ...
func StartServer() {
	app := iris.New()
  app.Handle("GET", "/user/{id:uint64}", func(ctx iris.Context) {
		id, _ := ctx.Params().GetUint64("id")
		ctx.JSON(id)
	})
  app.Listen(":8080")
}

// NewWebServer2 ...
func NewWebServer2() *iris.Application {
	app := iris.New()
  app.Handle("GET", "/user/{id:uint64}", func(ctx iris.Context) {
		id, _ := ctx.Params().GetUint64("id")
		ctx.JSON(id)
	})
	// curl -d '{"name":"ly", "city":"bj"}' -H "Content-Type: application/json" -X POST http://localhost:8080/user/123
	app.Handle("POST", "/user/{id:uint64}", func(ctx iris.Context) {
		id, _ := ctx.Params().GetUint64("id")
		var res struct{
			ID 		uint64 `json:id`
			Name  string `json:name`
			City  string `json:city`
		}
		if err := ctx.ReadJSON(&res); err != nil {
				ctx.StatusCode(iris.StatusBadRequest)
				ctx.WriteString(err.Error())
				return
		}
		res.ID = id
		ctx.JSON(res)
	})
  return app 
}

// StartServer2 ..
func StartServer2(){
	var port int 
	flag.IntVar(&port, "port", 8080, "Web端口默认8080")
	app := NewWebServer2()
	app.Listen(fmt.Sprintf(":%d", port))
}