module gitee.com/rxyk/go-easy/rixingyike/first

go 1.15

replace gitee.com/rxyk/go-easy/rixingyike/str v1.0.0 => ../str

require (
	gitee.com/rxyk/go-easy/rixingyike/str v1.0.0
	github.com/nleeper/goment v1.4.0
)