package fibonacci_test

import (
	"flag"
	"io/ioutil"
	"net/http"
	"os"
	"testing"
	"fmt"
	. "gitee.com/rxyk/go-easy/rixingyike/str"
	"github.com/kataras/iris/v12/httptest"
)

func TestMain(m *testing.M) {
	flag.Parse() // 解析可能需要的参数
	go func(){
		StartServer2()
	}()
	exitCode := m.Run()
	// 退出
	os.Exit(exitCode)
}

func ExampleGetUser123() {
	res, _ := http.Get("http://localhost:8080/user/123")
	resBody, _ := ioutil.ReadAll(res.Body)
	res.Body.Close()
	fmt.Printf("%s", resBody)
	// output:123
}

// 依托iris的httptest测试
func TestServerUser(t *testing.T) {
	app := NewWebServer2()
	e := httptest.New(t, app)
	e.GET("/user/123").Expect().Status(httptest.StatusOK).Body().Equal("123\n")
	e.POST("/user/123").WithBytes([]byte(`{"name":"ly","city":"bj"}`)).Expect().Status(httptest.StatusOK).Body().Equal("{\n  \"ID\": 123,\n  \"Name\": \"ly\",\n  \"City\": \"bj\"\n}\n")
}