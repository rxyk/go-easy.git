package fibonacci_test

import (
	. "gitee.com/rxyk/go-easy/rixingyike/str"
	"fmt"
)

// 各有妙用
func ExampleHello() {
	fmt.Println("Hello")
	// output: Hello
}

func ExampleFibonacci2() {
	var f = Fibonacci2()
	var res = 0
	for j := 0; j < 5; j++ {
		res = f()
	}
	fmt.Println(res)
	// output: 5
}