package fibonacci_test

import (
	. "gitee.com/rxyk/go-easy/rixingyike/str"
	"testing"
	"fmt"
	"github.com/stretchr/testify/assert"
)

func TestFibonacci_3(t *testing.T)  {
	t.Parallel()
	// 0，1，1，2，3，5，8，13
	for _, v := range []struct{
		in,expected int
	}{
		{1,1},
		{2,1},
		{3,2},
		{5,5},
		{7,13},
	} {
		t.Run(fmt.Sprintf("name%d",v.in), func(t *testing.T) {
			t.Parallel()
			res := Fibonacci(v.in)
			t.Logf("in:%d,res=%d\n",v.in, res)
			assert.Equal(t, v.expected, res)
		})
	}
}
