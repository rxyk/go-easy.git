package fibonacci_test

import (
	. "gitee.com/rxyk/go-easy/rixingyike/str"
	"testing"
)

// 基准测试
func BenchmarkFibonacci_10(b *testing.B) {
	for n := 0; n < b.N; n++ {
		Fibonacci(10) // 运行 Fibonacci 函数 N 次
	}
}

// 基准测试2
func BenchmarkFibonacci2_10(b *testing.B) {
	for n := 0; n < b.N; n++ {
		var f = Fibonacci2()
		for j := 0; j < 10; j++ {
			f()
		}
	}
}

// 基准测试3
func BenchmarkFibonacci_20(b *testing.B) {
	for n := 0; n < b.N; n++ {
		Fibonacci(20) // 运行 Fibonacci 函数 N 次
	}
}

// 基准测试4
func BenchmarkFibonacci2_20(b *testing.B) {
	for n := 0; n < b.N; n++ {
		var f = Fibonacci2()
		for j := 0; j < 20; j++ {
			f()
		}
	}
}