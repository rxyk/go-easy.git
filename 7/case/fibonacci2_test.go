package fibonacci_test

import (
	. "gitee.com/rxyk/go-easy/rixingyike/str"
	"testing"
)

func TestFibonacci_2(t *testing.T)  {
	t.Parallel()
	// 0，1，1，2，3，5，8，13
	for _, v := range []struct{
		in,expected int
	}{
		{1,1},
		{2,1},
		{3,2},
		{5,5},
		{7,13},
	} {
		if res := Fibonacci(v.in); res != v.expected {
			t.Errorf("2 Fibonacci(%d) == %d, want %d", v.in, res, v.expected)
		}
	}
}

func TestFibonacci2_2(t *testing.T)  {
	t.Parallel()
	// 0，1，1，2，3，5，8，13
	for _, v := range []struct{
		in,expected int
	}{
		{1,1},
		{2,1},
		{3,2},
		{5,5},
		{7,13},
	} {
		var f = Fibonacci2()
		var res = 0
		for j:=0;j<v.in;j++ {
			res = f()
		}
		if res != v.expected {
			t.Errorf("2 Fibonacci2(%d) == %d, want %d", v.in, res, v.expected)
		}
	}
}