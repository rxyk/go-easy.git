module str

go 1.15

replace gitee.com/rxyk/go-easy/rixingyike/str v1.0.0 => ../rixingyike/str

require (
	gitee.com/rxyk/go-easy/rixingyike/str v1.0.0
	github.com/kataras/iris/v12 v12.2.0-alpha.0.20210107033656-541fa75caf54
	github.com/stretchr/testify v1.6.1
)
