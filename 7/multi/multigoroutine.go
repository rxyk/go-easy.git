package main 

import (
	"gitee.com/rxyk/go-easy/rixingyike/str"
	"fmt"
	"time"
)

func main()  {
	for _, v := range []struct{
		in,expected int
	}{
		{1,10},
		{2,10},
		{3,20},
		{5,50},
		{7,13},
	} {
		// v := v 
		go func(){
			time.Sleep(time.Millisecond)
			res := str.Fibonacci(v.in)
			fmt.Printf("in:%d,res=%d\n",v.in, res)
		}()
	}
	// fmt.Printf("%v\n",v)
	// v是for循环退出后，被gc回收了，所以不能访问了
	time.Sleep(time.Second)
}