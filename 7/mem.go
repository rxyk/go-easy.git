package main 

import ("fmt")

// Cursor ...
type Cursor struct{
	X int 
}

func f() *Cursor {
	var c = new(Cursor)
	c.X = 500
	return c
}

func main()  {
	v := f()
	fmt.Printf("c=%v\n",v)
}