package main 

import ("sync")

var l sync.RWMutex
var a string = "hi"

func f() {
	// println(a)
	a = "hi, ly"
	l.Unlock() 
}

func main() {
	l.Lock() 
	go f()
	l.Lock() 
	println(a)
}