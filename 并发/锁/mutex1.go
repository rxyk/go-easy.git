package main 

import ("sync")

var l sync.Mutex
var a string

func f() {
	// println(a)
	a = "hi, ly"
	l.Unlock() // Unlock 方法解锁 m，如果 m 未加锁会导致运行时错误。
}

func main() {
	l.Lock() // 默认l是零值解锁状态，在这里先加锁
	go f()
	l.Lock() // l 已经加锁，则阻塞直到 l 解锁。
	println(a)
}