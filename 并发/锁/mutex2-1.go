package main

import (
	"fmt"
	"sync"
	"time"
)

func main() {
	var l sync.RWMutex
	var data = 1.0

	for i := 0; i < 10; i++ {
		go func(t int) {
			l.RLock()
			defer l.RUnlock()
			data += .1
			fmt.Printf("Read data: %d %v\n", t, data)
		}(i)
		// if i == 3 {
		// 	time.Sleep(time.Second)
		// }
		go func(t int) {
			l.Lock()
			defer l.Unlock()
			data++
			fmt.Printf("Write Data: %d %v \n", t, data)
		}(i)
	}
	time.Sleep(time.Second)
}