package main
import (
    "sync"
    "time"
)
func main() {// g2
    var mu sync.Mutex
    go func() { // g1
        mu.Lock() // step1
        time.Sleep(10 * time.Second) // 10''
        mu.Unlock() // step3,panic
    }()
    time.Sleep(time.Second) // 1''
    mu.Unlock() // step2
    select {} // 主线程没退，panic会显现
}

/*
如果一个goroutine g1 通过Lock获取了锁， 
在 g1 持有锁的期间， 另外一个goroutine g2 调用Unlock释放这个锁， 会出现什么现象？

A、 g2 调用 Unlock panic
B、 g2 调用 Unlock 成功，但是如果将来 g1调用 Unlock 会 panic
C、 g2 调用 Unlock 成功，如果将来 g1调用 Unlock 也成功

答案 B
*/