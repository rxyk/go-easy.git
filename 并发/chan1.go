package main 

var c = make(chan int, 1)
var a string

func f() {
	a = "hi, ly"
	c <- 0
}

func main() {
	go f() // 这里启动了一个Go程
	<-c
	println(a)
}