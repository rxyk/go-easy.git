package main

var c = make(chan int)
var a string

func f() {
	a = "hi, ly"
	c <- 0
}

func main() {
	go f()
	<-c
	println(a)
}