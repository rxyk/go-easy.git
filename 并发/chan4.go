package main

var c = make(chan int, 1)
var a string

func f() {
	a = "hi, world"
	<-c
}

func main() {
	go f()
	c <- 0
	println(a)
}